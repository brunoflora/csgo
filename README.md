Budi's Counter-Strike: Global Offensive Config




AUTOEXEC CONFIG

This is my constantly updated CS:GO autoexec config.

Put autoexec.cfg in ~/Library/Application Support/Steam/SteamApps/common/Counter-Strike Global Offensive/csgo/cfg or take what you want from it and add to your autoexec config!




LAUNCH OPTIONS

-novid -high -threads 4 -freq 144 -refresh 144 -tickrate 128 -nojoy -nod3d9ex +exec autoexec.cfg

Enter launch options at Steam > Library > Counter-Strike: Global Offensive (right-click) > Properties > Set Launch Options...